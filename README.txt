Tools needed: 
-git 
-node.js 
-npm

STEPS:
1. open console and cd into/some/folder/to/clone/repo
2. git clone https://zlamane_ucho@bitbucket.org/zlamane_ucho/stock_exchange.git
3. cd into/project/folder
4. type "npm install" and wait until process is done
5. type "npm start" then go to your browser and type "localhost:3000"