import React, { Component } from "react";
import { connect } from "react-redux";

class PrizesTable extends Component {
  buttonHandler = e => {
    this.props.showBuyModal(e.target.value);
  };

  makeTable() {
    const { auth } = this.props;
    return this.props.stockCompanies.map(item => {
      return (
        <tr key={item.code}>
          <th scope="col">{item.code}</th>
          <th scope="col">{item.price}</th>
          <th scope="col">{item.unit}</th>
          <th scope="col">
            {auth.uid ? (
              <button
                value={item.code}
                onClick={this.buttonHandler}
                className="btn btn-secondary btn-sm btn-const-width"
              >
                BUY
              </button>
            ) : (
              <p>Buy after signing in</p>
            )}
          </th>
        </tr>
      );
    });
  }

  render() {
    const { stockCompanies } = this.props;
    return (
      <div className="col-lg-6 col-sm-12">
        <table className="table">
          <thead>
            <tr>
              <th colSpan="3">
                <h6 className="display-4">Stock prizes</h6>
              </th>
              <th colSpan="1">
                <div>Values date: {this.props.date.toLocaleTimeString()}</div>
              </th>
            </tr>
            <tr>
              <th scope="col">Company</th>
              <th scope="col">Value</th>
              <th scope="col">Unit</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
            {stockCompanies.length ? (
              this.makeTable()
            ) : (
              <p>Waiting for data...</p>
            )}
          </tbody>
        </table>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    stockCompanies: state.exchange.stockCompanies,
    auth: state.firebase.auth,
    date: state.exchange.date,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    showBuyModal: stockName => dispatch({ type: "SHOW_BUY_MODAL", stockName })
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PrizesTable);
