import React, { Component } from 'react';
import PrizesTable from './PrizesTable';
import UserInfo from './UserInfo';

class MainInterface extends Component {
    render() {
        return (
            <div className="row row-mrg">
                <PrizesTable />
                <UserInfo />
            </div>
        );
    }
}

export default MainInterface;