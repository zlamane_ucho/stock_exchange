import React, { Component } from "react";
import { Link } from 'react-router-dom';
import LogedOutBtns from './LogedOutBtns';
import LogedInBtns from './LogedInBtns';
import { connect } from 'react-redux';

class NavBar extends Component {
  
  render() {
    const { auth } = this.props;

    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light justify-content-between">
        <Link className="navbar-brand link" to="/">
          STOCK EXCHANGE
        </Link>
        {auth.uid ? <LogedInBtns/> : <LogedOutBtns />}
      </nav>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
  }
}
export default connect(mapStateToProps)(NavBar);
