import React, { Component } from "react";
import {connect} from 'react-redux';
import {Redirect} from "react-router-dom";

import {signUp} from '../store/actions/authActions';

class RegisterForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      password: "",
      email: "",
      wallet: ""
    };

    this.inputHandler = this.inputHandler.bind(this);
    this.submitHandler = this.submitHandler.bind(this);
  }

  inputHandler(e) {
    let value = e.target.value;

    const isWalletValid = /^\d*\.?\d{0,2}$/.test(value);
    if(value.length > 20) return;
    if(e.target.id === "wallet" && (value.length > 10 || value === "0" || !isWalletValid)) return;
    this.setState({[e.target.id]: value})
  }

  submitHandler(e) {
    e.preventDefault();
    this.props.signUp(this.state);
    this.props.history.push('/');
  }

  render() {
    const { username, password, email, wallet } = this.state;
    const { auth } = this.props;

    if(auth.uid) return  <Redirect to="/"/>

    return (
      <div className="row justify-content-center row-mrg">
        <form onSubmit={this.submitHandler} className="col-lg-4">
          <div className="form-group">
            <label>Username</label>
            <input
              type="text"
              id="username"
              className="form-control"
              placeholder="Username"
              onChange={this.inputHandler}
              value={username}
            />
          </div>

          <div className="form-group">
            <label>Password</label>
            <input
              type="password"
              id="password"
              className="form-control"
              placeholder="Password"
              onChange={this.inputHandler}
              value={password}
            />
          </div>

          <div className="form-group">
            <label>Email address</label>
            <input
              type="email"
              id="email"
              className="form-control"
              placeholder="Enter email"
              onChange={this.inputHandler}
              value={email}
            />
          </div>

          <div className="form-group">
            <label>Money in wallet</label>
            <input
              type="text"
              id="wallet"
              className="form-control"
              placeholder="1500"
              onChange={this.inputHandler}
              value={wallet}
            />
          </div>

          <button type="submit" className="btn btn-primary">
            Register
          </button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    signUp: (user) => dispatch(signUp(user)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterForm);
