import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

import { updateAccount } from "../store/actions/userActions";

class AccountForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userInfo: {
        username: "",
        wallet: "",
        shares: {}
      }
    };

    this.inputHandler = this.inputHandler.bind(this);
    this.submitHandler = this.submitHandler.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.profile !== this.props.profile) {
      const { username, wallet, shares } = nextProps.profile;
      const userInfo = {
        username,
        wallet,
        shares
      };
      this.setState(state => {
        return {
          ...state,
          userInfo
        };
      });
    }
  }

  componentDidMount() {
    const { username, wallet, shares } = this.props.profile;
    const userInfo = {
      username,
      wallet,
      shares
    };
    this.setState(state => {
      return {
        ...state,
        userInfo
      };
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextState.userInfo !== this.state.userInfo || nextProps.profile !== this.props.profile;
  }

  inputHandler(e) {
    const userInfo = { ...this.state.userInfo };
    let value = e.target.value;

    const isWalletValid = /^\d*\.?\d*$/.test(value);
    if(value.length > 20) return;
    if(e.target.id === "wallet" && (value === "0" || !isWalletValid)) return;
    userInfo[e.target.id] = value;

    this.setState(state => {
      return {
        ...state,
        userInfo
      };
    });
  }

  submitHandler(e) {
    e.preventDefault();
    this.props.updateAccount(this.state.userInfo);
    this.props.history.push('/');
  }

  sharesHandler = e => {
    const shares = { ...this.state.userInfo.shares };

    shares[e.target.id] = e.target.value;

    this.setState(state => {
      return {
        ...state,
        userInfo: {
          ...state.userInfo,
          shares
        }
      };
    });
  };

  inlineCheckboxes = () => {
    const { stockCompanies } = this.props;
    const { shares } = this.state.userInfo;

    return stockCompanies.map(el => {
      let val;
      if (shares) val = shares[el.code];
      return (
        <div key={el.code} className="form-check form-check-inline">
          <input
            type="number"
            id={el.code}
            className="form-control"
            placeholder={el.code}
            value={val ? val : ""}
            onChange={this.sharesHandler}
          />
          <label className="form-check-label" htmlFor={el.code}>
            {el.code}
          </label>
        </div>
      );
    });
  };

  render() {
    const { username, wallet } = this.state.userInfo;
    const { auth, profile } = this.props;

    if(!auth.uid) return <Redirect to="/" />;
    if(!profile.username) return <p>"Czekam na dane"</p>;
    return (
      <div className="row justify-content-center row-mrg">
        <form onSubmit={this.submitHandler} className="col-lg-4">
          <div className="form-group">
            <label>Username</label>
            <input
              type="text"
              id="username"
              className="form-control"
              placeholder="Username"
              onChange={this.inputHandler}
              value={username}
            />
          </div>

          <div className="form-group">
            <label>Money in wallet</label>
            <input
              type="text"
              id="wallet"
              className="form-control"
              placeholder="1500"
              onChange={this.inputHandler}
              value={wallet}
            />
          </div>

          {this.inlineCheckboxes()}

          <button type="submit" className="btn btn-primary btn-block">
            Change account information
          </button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    stockCompanies: state.exchange.stockCompanies,
    profile: state.firebase.profile,
    auth: state.firebase.auth,
    date: state.exchange.date
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateAccount: user => dispatch(updateAccount(user))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AccountForm);
