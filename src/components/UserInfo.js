import React, { Component } from "react";
import { connect } from 'react-redux';

class UserInfo extends Component {
  makeTable() {
    const { shares } = this.props.profile;

    if(!shares) return null;
    return Object.keys(shares).map((item, iter) => {
      if(!shares[item]) return null;
      return (
        <tr key={iter}>
          <th scope="col">{item}</th>
          <th scope="col">{this.getSharePrice(item)}</th>
          <th scope="col">{shares[item]}</th>
          <th scope="col">{(shares[item] * this.getSharePrice(item)).toFixed(2)}</th>
          <th scope="col">
            <button onClick={this.buttonHandler} value={item} className="btn btn-secondary btn-sm btn-const-width">SELL</button>
          </th>
        </tr>
      );
    });
  }

  buttonHandler = (e) => {
    this.props.showSellModal(e.target.value)
  }


  shouldComponentUpdate(nextProps, nextState) {
     return nextProps !== this.props;
  }

  getSharePrice = item => {
    const {companies} = this.props;
    let price = -1;
    companies.forEach(el => {
      if(el.code === item) price = el.price;
    })
    return price;
  }

  render() {
    const { wallet } = this.props.profile;
    const { profile } = this.props;

    if(wallet){
      return (
        <div className="col-lg-6 col-sm-12">
          <table className="table">
            <thead>
              <tr>
                <th colSpan="5">
                  <h6 className="display-4">My wallet</h6>
                </th>
              </tr>
              {profile.shares && (<tr>
                <th scope="col">Company</th>
                <th scope="col">Unit price</th>
                <th scope="col">Amount</th>
                <th scope="col">Value</th>
                <th scope="col">Action</th>
              </tr>)}
            </thead>
            <tbody>{this.makeTable()}</tbody>
          </table>
          <h4>
            Available money: <span className="badge badge-secondary">{wallet}</span>
          </h4>
        </div>
      );
    } else if(profile.isLoaded) {
      return <h6 className="display-4">Sign in to your account</h6>
    } else {
      return <p>LOADING...</p>;
    }
    
  }
}

const mapStateToProps = state => {
  return {
    companies: state.exchange.stockCompanies,
    profile: state.firebase.profile
  }
}

const mapDispatchToProps = dispatch => {
  return {
    showSellModal: (stockName) => dispatch({type: "SHOW_SELL_MODAL", stockName})
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserInfo);
