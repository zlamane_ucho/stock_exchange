import React, { Component } from "react";
import { connect } from "react-redux";
import {Redirect} from "react-router-dom";

import { signIn } from "../store/actions/authActions";

class LoginForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      password: "",
      email: ""
    };

    this.inputHandler = this.inputHandler.bind(this);
    this.submitHandler = this.submitHandler.bind(this);
  }

  inputHandler(e) {
    this.setState({ [e.target.id]: e.target.value });
  }

  submitHandler(e) {
    e.preventDefault();
    this.props.signIn(this.state);
  }
  render() {
    const { password, email } = this.state;
    const { authError, auth } = this.props;

    if(auth.uid) return  <Redirect to="/"/>
    
    return (
      <div className="row justify-content-center row-mrg">
        <form onSubmit={this.submitHandler} className="col-lg-4">
          <div className="form-group">
            <label>Email address</label>
            <input
              type="email"
              id="email"
              className="form-control"
              placeholder="Enter email"
              onChange={this.inputHandler}
              value={email}
            />
          </div>

          <div className="form-group">
            <label>Password</label>
            <input
              type="password"
              id="password"
              className="form-control"
              placeholder="Password"
              onChange={this.inputHandler}
              value={password}
            />
            <small className="form-text text-muted">
              {authError ? authError : null}
            </small>
          </div>

          <button type="submit" className="btn btn-primary">
            Log In
          </button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    authError: state.auth.authError,
    auth: state.firebase.auth
  };
};

const mapDispatchToProps = dispatch => {
  return {
    signIn: cred => dispatch(signIn(cred))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginForm);
