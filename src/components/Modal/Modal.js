import React, {Component} from 'react';
import './Modal.css';
import Backdrop from '../Backdrop/Backdrop';

class Modal extends Component {

    shouldComponentUpdate(nextProps, nextState){
        return nextProps.show !== this.props.show || nextProps.children !== this.props.children;
    }

    modalToRender = () => {

        if(this.props.children === null) return null;
        else {
            let classes = "Modal";
            if(this.props.classes) classes = this.props.classes;
            return <div>
                <Backdrop show={this.props.show}/>
                <div className={classes}
                     style={{
                         transform: this.props.show ? 'translateY(0)' : 'translateY(-100vh)',
                         opacity: this.props.show ? '1' : '0'
                     }}>
                    <div>{this.props.children}</div>
                </div>
            </div>
        }
    };

    render() {
        return this.modalToRender();
    }
}

export default Modal;