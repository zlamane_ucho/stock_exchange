import React, { Component } from "react";
import { connect } from "react-redux";
import "../Modal.css";
import { sellStock } from "../../../store/actions/userActions";

class SellingModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      amount: "",
      error: []
    };

    this.onSubmit = this.onSubmit.bind(this);
    this.onCancel = this.onCancel.bind(this);
  }

  onSubmit(e) {
    e.preventDefault();
    const error = this.props.onSellSubmit(
      Number.parseInt(this.state.amount, 10)
    );
    if (error) {
      this.setState({ error });
    } else {
      this.setState({ amount: "", error: [] });
      this.props.hideSellModal();
    }
  }

  onCancel(e) {
    e.preventDefault();
    this.props.hideSellModal();
    this.setState({ amount: "", error: [] });
  }

  componentDidUpdate() {
    this.nameInput.focus();
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      nextProps.show !== this.props.show ||
      nextProps.children !== this.props.children ||
      nextState !== this.state
    );
  }

  inputHandler = e => {
    const value = e.target.value;
    const isValueValid = /^\d*$/.test(value);
    if(!isValueValid) return;
    this.setState({ [e.target.id]: e.target.value });
  };

  showErrors = () => {
    return this.state.error.map(el => {
      return <p key={el}>{el}</p>;
    });
  };

  render() {
    const { error } = this.state;
    return (
      <div className="row justify-content-center">
        <span className="modal-span">How much do you want to sell?</span> <br />
        <form onSubmit={this.onSubmit} onReset={this.onCancel}>
          <input
            id="amount"
            type="text"
            className="form-control form-eq"
            ref={input => {
              this.nameInput = input;
            }}
            placeholder="Amount"
            value={this.state.amount}
            onChange={this.inputHandler}
          />
          <small className="form-text text-muted">
            {error ? this.showErrors() : null}
          </small>
          <button type="submit" className="btn btn-success btn-eq">
            SELL
          </button>
          <button type="reset" className="btn btn-danger btn-eq">
            CANCEL
          </button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    activePurchasing: state.activePurchasing,
    stockName: state.exchange.stockName
  };
};

const mapDispatchToProps = dispatch => {
  return {
    hideSellModal: () => dispatch({ type: "HIDE_SELL_MODAL" }),
    onSellSubmit: amount => dispatch(sellStock(amount))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SellingModal);
