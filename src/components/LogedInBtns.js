import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { signOut } from "../store/actions/authActions";

class LogedInBtns extends Component {

  logoutHandler = (e) => {
    console.log("Logged out");
    this.props.signOut();
  }
  render() {
    const { profile } = this.props;
    return (
      <ul className="nav navbar-nav form-inline">
        <li className="nav-item">
          <button className="btn btn-sm btn-danger btn-mrg" disabled>
            Logged as: {profile.username}
          </button>
        </li>
        <li className="nav-item active">
          <NavLink to="/account">
            <button className="btn btn-success btn-sm btn-mrg btn-const-width">
              Account
            </button>
          </NavLink>
        </li>
        <li>
          <button
            onClick={this.logoutHandler}
            className="btn btn-primary btn-sm btn-mrg btn-const-width"
          >
            Logout
          </button>
        </li>
      </ul>
    );
  }
}

const mapStateToProps = state => {
  return {
    profile: state.firebase.profile
  };
};

const mapDispatchToProps = dispatch => {
  return {
    signOut: () => dispatch(signOut())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LogedInBtns);
