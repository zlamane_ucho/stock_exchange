import React, { Component } from "react";
import { NavLink } from "react-router-dom";

class LogedOutBtns extends Component {
  render() {
    return (
      <ul className="nav navbar-nav form-inline">
        <li className="nav-item active">
          <NavLink to="/login">
            <button className="btn btn-success btn-sm btn-mrg btn-const-width">
              Log In
            </button>
          </NavLink>
        </li>
        <li>
          <NavLink to="/register">
            <button className="btn btn-primary btn-sm btn-mrg btn-const-width">
              Sign Up
            </button>
          </NavLink>
        </li>
      </ul>
    );
  }
}

export default LogedOutBtns;
