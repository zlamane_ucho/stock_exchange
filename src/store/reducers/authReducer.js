const initState = {

    authError: null

};

const authReducer = (state = initState, action) => {
  switch (action.type) {
    case "SIGNUP_SUCCESS":
      console.log("Signed up");
      return { ...state };
    case "SIGNUP_ERROR":
      console.log("ERROR SIGN UP", action.err.message);
      return { ...state };
    case "LOGIN_SUCCESS":
      console.log("Logged in");
      return { ...state, authError: null };
    case "LOGIN_ERROR":
      console.log("ERROR LOGIN", action.err.message);
      return { ...state, authError: action.err.message };
    default:
      return { ...state };
  }
};

export default authReducer;
