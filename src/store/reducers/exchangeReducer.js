const initState = {
  stockCompanies: [],
  showBuying: false,
  showSelling: false,
  stockName: "",
  date: new Date(),
  valuesHistory: {}
};

const exchangeReducer = (state = initState, action) => {
  switch (action.type) {
    case "SHOW_BUY_MODAL":
      return {
        ...state,
        showBuying: true,
        stockName: action.stockName
      };
    case "HIDE_BUY_MODAL":
      return {
        ...state,
        showBuying: false
      };
    case "SHOW_SELL_MODAL":
      return {
        ...state,
        showSelling: true,
        stockName: action.stockName
      };
    case "HIDE_SELL_MODAL":
      return {
        ...state,
        showSelling: false
      };
    case "GET_STOCK_PRICES":

      //Get history values
      let tempValuesHistory = {...state.valuesHistory};
      if (!Object.keys(tempValuesHistory).length) {
        action.companies.forEach(el => {
          tempValuesHistory[el.code] = [el.price];
        })
        console.log(tempValuesHistory);
      }
      
      action.companies.forEach(el => {
        let tempArr = tempValuesHistory[el.code];
        if(el.price !== tempArr[tempArr.length-1]){
          tempArr.push(el.price);
          console.log(tempValuesHistory);
        }
      })

      let keys = Object.keys(tempValuesHistory);
      
      keys.forEach(key => {
        if(tempValuesHistory[key].length > 20) {
          tempValuesHistory[key].splice(0,1);
        }
      })
      

      return {
        ...state,
        stockCompanies: action.companies,
        date: new Date(),
        valuesHistory: tempValuesHistory
      };
    default:
      return { ...state };
  }
};

export default exchangeReducer;
