import authReducer from './authReducer';
import exchangeReducer from './exchangeReducer';
import { combineReducers } from 'redux';
import { firestoreReducer } from 'redux-firestore';
import { firebaseReducer } from 'react-redux-firebase';

const rootReducer = combineReducers({
    auth: authReducer,
    exchange: exchangeReducer,
    firestore: firestoreReducer,
    firebase: firebaseReducer
});

export default rootReducer;