export const updateAccount = (account) => {
    return (dispatch, getState, {getFirestore}) => {
        const firestore = getFirestore();

        const state = getState();
        const uid = state.firebase.auth.uid


        const userProfile = state.firebase.profile;

        let newProfile = {...userProfile, ...account};

        const doc = firestore.collection('users').doc(uid);
        
        doc.set(newProfile, {merge: true})
        .then(() => console.log("Updated account"))
        .catch(err => console.log(err.message));
    }
}

export const buyStock = (amount) => {
    return (dispatch, getState, {getFirestore}) => {
        const firestore = getFirestore();
        const state = getState();

        let errors = [];

        //uid for DB
        const uid = state.firebase.auth.uid
        //user's information
        const userProfile = state.firebase.profile;
        //stock that is buying
        const stockName = state.exchange.stockName;     
        
        //check for stock price and unit
        let unit = 1;
        let stockPrice = 0;
        state.exchange.stockCompanies.forEach(el => {
            if(el.code === stockName) {
                stockPrice = el.price;
                unit = el.unit;
            }
        })

        //calculate money in wallet
        let wallet = (userProfile.wallet - amount*stockPrice).toFixed(2);
        if(wallet < 0) errors.push("Not enough money in wallet");

        //chech if amount is unit multiply
        if(amount%unit){
            errors.push(`Amount isn't multiply of ${unit}`)
        }

        if(errors.length) return errors;

        //if is valid prepare object to set
        let shares = {...userProfile.shares};
        //check if user have had this shares in the past
        if(shares[stockName]) shares[stockName] = Number.parseInt(shares[stockName], 10) + amount;
        else shares[stockName] = amount;
        
        let newProfile = {
            ...userProfile,
            wallet,
            shares
        };

        //send object to DB
        const doc = firestore.collection('users').doc(uid);
        doc.set(newProfile, {merge: true})
        .then(() => console.log("Updated account"))
        .catch(err => console.log(err.message));

    }
}

export const sellStock = (amount) => {
    return (dispatch, getState, {getFirestore}) => {
        const firestore = getFirestore();
        const state = getState();

        let errors = [];

        //uid for DB
        const uid = state.firebase.auth.uid
        //user's information
        const userProfile = state.firebase.profile;
        //stock that is buying
        const stockName = state.exchange.stockName;     
        
        //check for stock price and unit
        let stockPrice = 0;
        state.exchange.stockCompanies.forEach(el => {
            if(el.code === stockName) {
                stockPrice = el.price;
            }
        })

        
        //calculate money in wallet
        let wallet = (Number.parseInt(userProfile.wallet, 10) + amount*stockPrice).toFixed(2);

        
        //chech if amount correct
        let shares = {...userProfile.shares};
        if(amount > Number.parseInt(shares[stockName], 10)){
            errors.push(`You have only ${Number.parseInt(shares[stockName], 10)} shares`);
        }

        if(errors.length) return errors;


        //if is valid prepare object to set
        shares[stockName] = Number.parseInt(shares[stockName], 10) - amount;

        let newProfile = {
            ...userProfile,
            wallet,
            shares
        };

        //send object to DB
        const doc = firestore.collection('users').doc(uid);
        doc.set(newProfile, {merge: true})
        .then(() => console.log("Updated account"))
        .catch(err => console.log(err.message));

    }
}