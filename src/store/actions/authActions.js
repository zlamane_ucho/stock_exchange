export const signUp = (user) => {
    return (dispatch, getState, {getFirebase, getFirestore}) => {
        const firebase = getFirebase();
        const firestore = getFirestore();

        firebase.auth().createUserWithEmailAndPassword(
            user.email,
            user.password
        ).then(res => {
            return firestore.collection('users').doc(res.user.uid).set({
                username: user.username,
                wallet: user.wallet
            })
        }).then(() => {
            dispatch({type: 'SIGNUP_SUCCESS'});
        }).catch(err => {
            dispatch({type: 'SIGNUP_ERROR', err})
        })
    }
}

export const signIn = (cred) => {
    return (dispatch, getState, {getFirebase}) => {
        const firebase = getFirebase();

        firebase.auth().signInWithEmailAndPassword(
            cred.email,
            cred.password
        ).then(() => {
            dispatch({type: 'LOGIN_SUCCESS'});
        }).catch(err => {
            dispatch({type:'LOGIN_ERROR', err})
        })
    }
}

export const signOut = () => {
    return (dispatch, getState, {getFirebase}) => {
        const firebase = getFirebase();

        firebase.auth().signOut().then(() => console.log("Signed Out"));
    }
}