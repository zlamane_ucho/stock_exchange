export const getStockPrices = () => {
  return (dispatch, getState) => {
    fetch("/stocks")
    .then(res => res.json())
    .then(data => dispatch({type: "GET_STOCK_PRICES", companies: data.items}));
  };
};
