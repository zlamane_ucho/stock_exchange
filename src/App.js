import React, { Component } from "react";
import NavBar from "./components/NavBar";
import RegisterForm from "./components/RegisterForm";
import LoginForm from "./components/LoginForm";
import AccountForm from "./components/AccountForm";
import MainInterface from "./components/MainInterface";

import Modal from './components/Modal/Modal';
import BuyingModal from './components/Modal/ModalBodies/BuyingModal';
import SellingModal from "./components/Modal/ModalBodies/SellingModal";
import { getStockPrices } from "./store/actions/stockActions";

import { BrowserRouter, Route } from "react-router-dom";
import { connect } from 'react-redux';

import "./components.css";


class App extends Component {
  componentDidMount(){
    this.timerID = setInterval(() => {
      this.props.getStockPrices();
    }, 1000);
  }

  render() {
    return (
      <div className="container">
        <Modal show={this.props.showBuying}>
          <BuyingModal />
        </Modal>
        <Modal show={this.props.showSelling}>
          <SellingModal />
        </Modal>
        <BrowserRouter>
          <div>
            <Route path="/" component={NavBar} />
            <Route path="/account" component={AccountForm} />
            <Route path="/login" component={LoginForm} />
            <Route path="/register" component={RegisterForm} />
            <Route path="/" component={MainInterface} exact/>
          </div>
        </BrowserRouter>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return{
    showBuying: state.exchange.showBuying,
    showSelling: state.exchange.showSelling
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getStockPrices: () => dispatch(getStockPrices())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
