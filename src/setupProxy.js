const proxy = require('http-proxy-middleware');

module.exports = function(app) {
  app.use(proxy('/stocks', { target: 'http://webtask.future-processing.com:8068/' }));

  app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
};