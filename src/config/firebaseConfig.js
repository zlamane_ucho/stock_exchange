import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'

var config = {
    apiKey: "AIzaSyD8L47LmQ8LSA6tTAV25dQXB669Kk9KiOY",
    authDomain: "stock-exchange-fp.firebaseapp.com",
    databaseURL: "https://stock-exchange-fp.firebaseio.com",
    projectId: "stock-exchange-fp",
    storageBucket: "stock-exchange-fp.appspot.com",
    messagingSenderId: "1083499707983"
  };


firebase.initializeApp(config);

export default firebase;